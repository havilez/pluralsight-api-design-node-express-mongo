// TODO: create a basic server with express
// that will send back the index.html file on a GET request to '/'
// it should then send back jsonData on a GET to /data

const http = require('http');
const fs = require('fs');




let jsonData = {count: 12, message: 'hey'};

const express = require('express');

const app = express();


// on GET request to the url
app.get('/',function(req,res) {
    // read index file
    // res.sendFile does the fs.readFile internally
//   res.sendFile(__dirname+'/index.html', function(err){
//         if ( err ) {
//             res.status(500).send(err);
//         }
//     });

    fs.readFile('index.html',function(err,buffer){
        var html = buffer.toString();
        res.setHeader('Content-Type', 'text/html')
        res.send(html);
    })
  });


app.get('/data',function(req,res){
    res.json(jsonData);
});


app.get('/todos', function(req, res) {

});

// on POST request to the same url
app.post('/todos', function(req, res) {

});

// start server on port 3000
app.listen(3000);
